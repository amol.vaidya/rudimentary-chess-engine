import chess
import sys
import random
import pdb
from matrices import white_knight_matrix, white_pawn_matrix, white_bishop_matrix, \
                     black_knight_matrix, black_pawn_matrix, black_bishop_matrix
import pprint

BISHOP_VALUE = 3
KNIGHT_VALUE = 3
ROOK_VALUE = 5
QUEEN_VALUE = 9
depth = 4

def debug():
    import traceback
    print("Type q to quit debugger")
    while True:
        inp = input("Debug: ")
        if inp.lower() in ["q", "quit", "exit"]:
            break
        try:
            exec(inp)
        except Exception as err:
            print(traceback.format_exc())

def evaluate(m, d, cp):
    cp.push(m)
    if cp.is_checkmate():
        if cp.outcome().winner == chess.WHITE:
            return 100
        else:
            return -100
    if d == 0:
        fen = cp.fen().split()[0]
        eval = - fen.count('p') \
               - fen.count('b') * BISHOP_VALUE \
               - fen.count('n') * KNIGHT_VALUE \
               - fen.count('r') * ROOK_VALUE \
               - fen.count('q') * QUEEN_VALUE \
               + fen.count('P') \
               + fen.count('B') * BISHOP_VALUE \
               + fen.count('N') * KNIGHT_VALUE \
               + fen.count('R') * ROOK_VALUE \
               + fen.count('Q') * QUEEN_VALUE
        r = 7
        for row in fen.split("/"):
            c = 0
            for ch in row:
               if ch.isdigit():
                   c = c + int(ch)
                   continue
               if ch == "p":
                   eval = eval + black_pawn_matrix[r][c]
               elif ch == "P":
                   eval = eval + white_pawn_matrix[r][c]
               elif ch == "n":
                   eval = eval + black_knight_matrix[r][c]
               elif ch == "N":
                   eval = eval + white_knight_matrix[r][c]
               elif ch == "b":
                   eval = eval + black_bishop_matrix[r][c]
               elif ch == "B":
                   eval = eval + white_bishop_matrix[r][c]
               c = c+1
            r = r - 1
        return eval
    else:
        if cp.turn == chess.WHITE:
            return max([evaluate(q, d-1, cp.copy()) for q in list(cp.legal_moves)])
        else:
          return min([evaluate(q, d-1, cp.copy()) for q in list(cp.legal_moves)])

board= chess.Board()
while True:
    if board.turn == chess.WHITE:
        while True:
            m = input("Enter move: ")
            if m in [board.san(x) for x in list(board.legal_moves)]:
                board.push_san(m)
                break
            elif m.lower() in ['q', 'quit', 'exit']:
                sys.exit()
            elif m.lower() == "debug":
                debug()
            else:
                print("Please enter a valid move")
                print([board.san(x) for x in list(board.legal_moves)])      
    elif board.turn == chess.BLACK:
        evaluations = dict(zip(list(board.legal_moves), [evaluate(m, depth-1, board.copy()) for m in list(board.legal_moves)]))
        pprint.pprint(evaluations)
        min_eval = min(evaluations.values())
        min_keys = [k for k in evaluations if evaluations[k] == min_eval]
        computer_move = random.choice(min_keys)
        print("Computer moves {}".format(computer_move))
        board.push(computer_move)
        print(board)
        print()
    if len(list(board.legal_moves)) < 1:
        break

